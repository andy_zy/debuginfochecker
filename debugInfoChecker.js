(function(){
	var campaignScripts = [].slice.call(document.querySelectorAll('.mm-CampaignLocations .value-block .script')),
		altVariants = [].slice.call(document.querySelectorAll('.mm-CampaignContent a.mm-value')),
		siteScripts = [].slice.call(document.querySelectorAll('.mm-edit[href*="/DomainScripts/Edit/"]')),
		checkedItems = [],
		cycle = function(arr, data){
			arr.forEach(function(el){
				sendRequest(el, data.item, data.itemType, data.callback)
			});
		},
		parseRequestUrl = function(item, itemType, url){
			var firstPartUrl,
				lastPartUrl,
				requestUrl;

			if(itemType === 'DomainScript'){
				requestUrl = url;
			} else {
				firstPartUrl = (url.match(/^.+campaign\-/) || [])[0];
				lastPartUrl = (url.match(/(\d+)\/Campaign/) || [])[1];
				requestUrl = firstPartUrl + 'none/' + lastPartUrl + item;
			}

			return requestUrl;
		},
		sendRequest = function(el, item, itemType, callback){
			var url = el.href,
				types = {
					'Script': 'campaignScriptId',
					'HtmlContent': 'variantId'
				},
				xsrfToken = document.cookie.match(/XSRF-TOKEN=(.[^;]+)/ || [,''])[1],
				itemId = (url.match(/\d+$/) || [])[0],
				requestUrl = parseRequestUrl(item, itemType, url),
				xhr = new XMLHttpRequest(),
				doRequest = function(method, reqUrl, contentType, dataToSend, callback){
					xhr.open(method, reqUrl, true);
					xhr.setRequestHeader('Content-Type', contentType);
					xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					xhr.setRequestHeader('X-XSRF-TOKEN', xsrfToken);
					xhr.send(dataToSend);
					xhr.onload = function(data){
						typeof callback === 'function' && callback(data, itemType);
					};
				},
				dataToSend = {};

			if(!~checkedItems.indexOf(url)){
				checkedItems.push(url);
				if(requestUrl){
					if(itemType === 'DomainScript'){
						doRequest('GET', requestUrl, 'text/html', null, callback);
					} else {
						dataToSend[types[itemType]] = itemId;
						doRequest('POST', requestUrl, 'application/json', JSON.stringify(dataToSend), callback);
					}
				} else {
					console.info(el.innerHTML + ' LINK is not valid!');
				}
			}
		},
		parseJSON = function(string){
			var output = {};

			try{
				output = JSON.parse(string)
			}
			catch(e){
				console.info('JSON is not valid!');
			}

			return output;
		},
		findDebugEntries = function(itemName, itemType, itemData){
			var resultsArr = [],
				namePrefix = {
					'Script': 'Script',
					'HtmlContent': 'Variant',
					'DomainScript': 'SiteScript'
				};

			if(/debugger|(alert|console\.(log|info|warn|error|debug))\s*\(/.test(itemData)){
				resultsArr.push(namePrefix[itemType] + ': ' + itemName + ' contains DEBUGGER/CONSOLE/ALERT entries!')
			}
			if(/devel\.maxymiser\.com/.test(itemData)){
				resultsArr.push(namePrefix[itemType] + ': ' + itemName + ' contains assets placed on the DEVEL!')
			}

			if(resultsArr.length){
				console.warn(resultsArr.join('\n'));
			} else {
				console.info(namePrefix[itemType] + ': ' + itemName + ' code is ready for production.');
			}
		},
		responseHandler = function(data, itemType){
			var responseText = data && data.target && data.target.responseText,
				extractAreaForCheck = function(response){
					var dataToReturn = {},
						divWrap,
						nameTag,
						scriptTag;

					if(itemType === 'DomainScript'){
						divWrap = document.createElement('div');
						divWrap.innerHTML = response;
						nameTag = divWrap.querySelector('#Name');
						scriptTag = divWrap.querySelector('#Script');
						dataToReturn.Name = nameTag && nameTag.value;
						dataToReturn.DomainScript = scriptTag && scriptTag.innerHTML;
					} else {
						dataToReturn = parseJSON(responseText);
					}

					return dataToReturn;
				},
				itemData = extractAreaForCheck(responseText);

			if(itemData){
				findDebugEntries(itemData.Name, itemType, itemData[itemType]);
			} else {
				console.info('SiteScript data parsing error!');
			}
		};

	if(campaignScripts.length){
		cycle(campaignScripts, {
			'item': '/CampaignScripts/GetCampaignScript',
			'itemType': 'Script',
			'callback': function(data, itemType){
				responseHandler(data, itemType);
			}
		});
	}

	if(altVariants.length){
		cycle(altVariants, {
			'item': '/CampaignContentVariant/GetVariant',
			'itemType': 'HtmlContent',
			'callback': function(data, itemType){
				responseHandler(data, itemType);
			}
		});
	}

	if(siteScripts.length){
		cycle(siteScripts, {
			'item': null,
			'itemType': 'DomainScript',
			'callback': function(data, itemType){
				responseHandler(data, itemType);
			}
		});
	}
})();