# USER MANUAL #


## Description ##

Script dedicated for searching debug entries in the Campaign assets.

## Browser Compability ##

Chrome(All), FF(All), Safari(All), IE(9+)

## How to install ##

You can install it in next way:

* Add next code to the existing bookmarklet URL field or use code from the direct [LINK](https://bitbucket.org/andy_zy/debuginfochecker/raw/5484eba5281dd9921af471cadd54c3ab2abba191/debugInfoCheckerBookmarklet.js).

```
#!javascript

javascript:(function(){var f=[].slice.call(document.querySelectorAll(".mm-CampaignLocations .value-block .script")),g=[].slice.call(document.querySelectorAll(".mm-CampaignContent a.mm-value")),m=[].slice.call(document.querySelectorAll('.mm-edit[href*="/DomainScripts/Edit/"]')),n=[],k=function(c,a){c.forEach(function(b){p(b,a.item,a.itemType,a.callback)})},q=function(c,a,b){"DomainScript"===a?c=b:(a=(b.match(/^.+campaign\-/)||[])[0],b=(b.match(/(\d+)\/Campaign/)||[])[1],c=a+"none/"+b+c);return c},p=function(c,a,b,e){var d=c.href,r={Script:"campaignScriptId",HtmlContent:"variantId"},k=document.cookie.match(/XSRF-TOKEN=(.[^;]+)/ || [,''])[1],l=(d.match(/\d+$/)||[])[0];a=q(a,b,d);var h=new XMLHttpRequest,f=function(a,c,d,e,f){h.open(a,c,!0);h.setRequestHeader("Content-Type",d);h.setRequestHeader("X-Requested-With","XMLHttpRequest");h.setRequestHeader("X-XSRF-TOKEN",k);h.send(e);h.onload=function(a){"function"===typeof f&&f(a,b)}},g={};~n.indexOf(d)||(n.push(d),a?"DomainScript"===b?f("GET",a,"text/html",null,e):(g[r[b]]=l,f("POST",a,"application/json",JSON.stringify(g),e)):console.info(c.innerHTML+" LINK is not valid!"))},l=function(c,a){var b=c&&c.target&&c.target.responseText,e={},d;if("DomainScript"===a)d=document.createElement("div"),d.innerHTML=b,b=d.querySelector("#Name"),d=d.querySelector("#Script"),e.Name=b&&b.value,e.DomainScript=d&&d.innerHTML;else{e={};try{e=JSON.parse(b)}catch(f){console.info("JSON is not valid!")}}if(b=e){e=b.Name;b=b[a];d=[];var g={Script:"Script",HtmlContent:"Variant",DomainScript:"SiteScript"};/debugger|(alert|console\.(log|info|warn|error|debug))\s*\(/.test(b)&&d.push(g[a]+": "+e+" contains DEBUGGER/CONSOLE/ALERT entries!");/devel\.maxymiser\.com/.test(b)&&d.push(g[a]+": "+e+" contains assets placed on the DEVEL!");d.length?console.warn(d.join("\n")):console.info(g[a]+": "+e+" code is ready for production.")}else console.info("SiteScript data parsing error!")};f.length&&k(f,{item:"/CampaignScripts/GetCampaignScript",itemType:"Script",callback:function(c,a){l(c,a)}});g.length&&k(g,{item:"/CampaignContentVariant/GetVariant",itemType:"HtmlContent",callback:function(c,a){l(c,a)}});m.length&&k(m,{item:null,itemType:"DomainScript",callback:function(c,a){l(c,a)}})})();

```

## How it works ##
 
1.Go to the appropriate Campaign or Site Scripts page in the UI.

------------------------------ 

![2015-06-24_1523.png](https://bitbucket.org/repo/RK5ddk/images/623055492-2015-06-24_1523.png)

![site-scripts.png](https://bitbucket.org/repo/RK5ddk/images/1344500515-site-scripts.png)

------------------------------ 

**NOTE!** Site scripts list for checking consist of the scripts which are presented on the page. If there is more then 20 site scripts you need to change filter option from 20 to 50 or run DevelChecker on each page separately.

------------------------------ 

![2015-08-19_1536.png](https://bitbucket.org/repo/RK5ddk/images/40106299-2015-08-19_1536.png)

------------------------------ 

2.Open browser console(F12) and Console tab.

------------------------------ 

![2015-06-24_1524.png](https://bitbucket.org/repo/RK5ddk/images/3091184421-2015-06-24_1524.png)

------------------------------ 

3.Run Bookmarklet

------------------------------ 

![2015-06-24_1527.png](https://bitbucket.org/repo/RK5ddk/images/1566956859-2015-06-24_1527.png)

------------------------------ 

Script will check debug entries(**devel.maxymiser.com, alert, debugger, console[log,warn,error,debug,info]**) in the source code of the each Campaign script and Alternative variant.
If such entries are exist they will be highlighted in the browser console in the format **[ITEM_NAME] [ENTRY_TYPE]** (e.g. "T20_Utils contains assets placed on the DEVEL!") if not - item will be marked as checked(e.g "T20_Render code is ready for production.")

------------------------------ 

![2015-06-26_1131.png](https://bitbucket.org/repo/RK5ddk/images/1018656463-2015-06-26_1131.png)